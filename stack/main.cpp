#include <iostream>

class Stack {
    public:
        Stack(int maxSize) {
            this->_array = new int[maxSize];
            this->_top   = -1;
            this->_max   = maxSize;
        }

        int peek() {
            if (_top >= 0) {
                return this->_array[this->_top];
            } else {
                return -1;
            }
        }

        int push(int item) {
            if (this->_top + 1 >= this->_max) {
                return -1;
            } else {
                this->_array[_top + 1] = item;

                return 0;
            }
        }

        int pop() {
            if (this->_top < 0) {
                return -1;
            } else {
                return this->_array[this->_top];
            }
        }

    private:
        int* _array;
        int  _top;
        int  _max;

};

int main() {
    return 0;
}
