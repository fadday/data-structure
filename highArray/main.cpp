#include <iostream>


class Array {

private:
    int* a;
    int count;

public:
    Array (int size) {
        a = new int[size];
        count = 0;
    }

    ~Array () {
        delete a;
    }

    int getMax () {
        if (count != 0)
            return a[count - 1];
        else
            return -1;
    }

    void removeMax () {
        if (count != 0) {
            count--;
        } else {
            return;
        }
    }

    void insert (int item) {
        if (count == 0) {
            a[0] = item;
        } else {
            for (int i = 0; i < count; i++) {
                if (item < a[i]) {
                    for (int j = count; j > i; j--) {
                        a[j] = a[j - 1];
                    }

                    a[i] = item;

                    break;
                }

                if (i == count - 1) {
                    a[count] = item;
                }
            }
        }

        count++;
    }

    void remove (int item) {
        int index = find(item);

        for (index; index < count - 1; index++) {
            a[index] = a[index + 1];
        }

        count--;
    }

    int find (int item) {
        int lowerBound = 0;
        int upperBound = count - 1;
        int curIndex;


        while (true) {
            curIndex = (lowerBound + upperBound) / 2;

            if (item == a[curIndex]) {
                return curIndex;
            } else if (lowerBound > upperBound) {
                return -1;
            } else {
                if (item < a[curIndex]) {
                    upperBound = curIndex - 1;
                } else {
                    lowerBound = curIndex + 1;
                }
            }
        }
    }

    void print () {
        for (int i = 0; i < count; i++) {
            std::cout << a[i] << std::endl;
        }
    }
};

int main() {
    Array* array = new Array(10);

    array->insert(15);
    array->insert(25);
    array->insert(10);
    array->insert(20);
    array->insert(5);
    array->insert(30);
    array->insert(45);
    array->insert(35);
    array->insert(40);

    array->print();

    std::cout << array->find(10) << std::endl;
    std::cout << array->find(25) << std::endl;

    array->remove(10);
    array->remove(20);

    std::cout << array->find(10) << std::endl;
    std::cout << array->find(15) << std::endl;

    array->print();

    array->removeMax();
    array->print();

    std::cout << array->getMax() << std::endl;

    delete array;
}
