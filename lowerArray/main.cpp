#include <iostream>

class Array {

private:
    int* a;
    int count;
    int test;

public:
    Array (int size) {
        a = new int[size];
        count = 0;
    }

    void insert (int item) {
        a[count] = item;
        count++;
    }

    void remove (int item) {
        int index = find(item);

        for (index; index < count - 1; index++) {
            a[index] = a[index + 1];
        }

        count--;
    }

    int find (int item) {
        for (int i = 0; i < count; i++) {
            if (a[i] == item) {
                return i;
            }
        }

        return -1;
    }

    void print () {
        for (int i = 0; i < count; i++) {
            std::cout << a[i] << std::endl;
        }
    }

    void boobleSort () {
        int last = count - 1;
        
        for (int i = count - 1; i > 0; i--) {
            for (int j = 0; j < i; j++) {
                if (a[j] > a[j + 1]) {
                    int temp = a[j + 1];
                    a[j + 1] = a[j];
                    a[j] = temp;
                }
            }
        }
    }

    void insertSort () {
        int marker = 1;

        for (marker; marker < count; marker++) {
            int temp = a[marker];
            int checking = marker;

            while(temp < a[checking - 1] || checking < 0) {

                a[checking] = a[checking - 1];

                checking--;
            }

            a[checking] = temp;
        }
    }

    void selectSort() {
        int start = 0;

        for (int start = 0; start < count; start++) {
            int min = start;

            for (int i = start; i < count; i++) {
                if (a[i + 1] < a[min]) {
                    min = i + 1;
                }
            }

            int temp = a[min];
            a[min] = a[start];
            a[start] = temp;
        }
    }
};

int main() {
    Array* array = new Array(500000);

    for (int i = 30000; i > 0; i--) {
        array->insert(i);
    }

    std::cout << "Start sorting..." << std::endl;

    array->selectSort();

    std::cout << "Done!" << std::endl;
}
